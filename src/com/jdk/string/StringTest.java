package com.jdk.string;

import org.junit.Test;

import java.lang.reflect.Field;

public class StringTest {
    public static void main(String[] args) {
        //字面量，只会在常量池中创建对象
        String str1 = "hello";
        String str2 = str1.intern();
        //true
        System.out.println(str1 == str2);

        //new 关键字只会在堆中创建对象
        String str3 = new String("world");
        String str4 = str3.intern();
        //false
        System.out.println(str3 == str4);

        //变量拼接的字符串，会在常量池中和堆中都创建对象
        String str5 = str1 + str2;
        //这里由于池中已经有对象了，直接返回的是对象本身，也就是堆中的对象
        String str6 = str5.intern();
        //true
        System.out.println(str5 == str6);

        //常量拼接的字符串，只会在常量池中创建对象
        String str7 = "hello1" + "world1";
        String str8 = str7.intern();
        //true
        System.out.println(str7 == str8);
    }

    @Test
    public void test01() throws NoSuchFieldException, IllegalAccessException {
        String str = "gujm";
        System.out.println(str);
        Field value = String.class.getDeclaredField("value");
        value.setAccessible(true);
        char[] charValue = (char[]) value.get(str);
        charValue[0] = 'G';
        System.out.println(str);
    }

    @Test
    public void test02() {
        String test = "一二三四五六";
        test = test.length() < 18 ? test : test.substring(0, 18);
        System.out.println(test);
    }


}
