package com.jdk.string;

import java.lang.reflect.Field;

public class StringTest2 {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        String str = "gujm";
        //打印原字符串
        System.out.println(str);    //gujm
        //获取String类中的value字段
        Field value = String.class.getDeclaredField("value");
        //因为value是private声明的，这里修改其访问权限
        value.setAccessible(true);
        //获取str对象上的value属性的值
        char[] charValue = (char[]) value.get(str);
        //将第一个字符修改为 V(小写改大写)
        charValue[0] = 'G';
        //打印修改之后的字符串
        System.out.println(str);    //Gujm
    }
}
