package com.jdk.string;

public class Stringtest3 {
    public static void main(String[] args) {
        String test = "一二三四五六一二三四五六一二三四五六一二三四五六";
        test = test.length() < 18 ? test : test.substring(0, 18);
        System.out.println(test);
    }
}
