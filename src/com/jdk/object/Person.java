package com.jdk.object;

import java.util.Objects;

public class Person {
    private String pname;
    private int page;

    public Person(String pname, int page) {
        this.pname = pname;
        this.page = page;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public boolean equals(Object o) {
        //引用相等那么两个对象当然相等
        if (this == o) return true;
        //对象为空或者不是Person类的实例
//        if (o == null || !(o instanceof Person)) return false;
        if (o == null || (getClass() != o.getClass())) return false;
        Person person = (Person) o;
        return page == person.page &&
                Objects.equals(pname, person.pname);
    }

    public static void main(String[] args) {
        Person p1 = new Person("Tom", 21);
        Person p2 = new Person("Marry", 20);
        System.out.println(p1 == p2);//false
        System.out.println(p1.equals(p2));//false

        Person p3 = new Person("Tom", 21);
        System.out.println(p1 == p3);//false，也就是判断的是p1、p3在栈内存中的地址是否相等
        System.out.println(p1.equals(p3));//true
    }
}
