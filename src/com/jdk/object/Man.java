package com.jdk.object;

import java.util.Objects;

public class Man extends Person {
    private String sex;

    public Man(String pname, int page, String sex) {
        super(pname, page);
        this.sex = sex;
    }

    @Override
    public boolean equals(Object o) {
        //判断本身的引用
        System.out.println(o.getClass());
        if (this == o) return true;
        if (!super.equals(o)) return false;
        if (o == null || !(o instanceof Man)) return false;
        Man man = (Man) o;
        return Objects.equals(sex, man.sex);
    }

    public static void main(String[] args) {
        Person p = new Person("Tom", 22);
        Man m = new Man("Tom", 22, "男");

        System.out.println(p.equals(m));//true
        System.out.println(m.equals(p));//false
    }

}
