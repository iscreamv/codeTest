package com.code.queue;

public class QueueApp {

    public static void main(String[] args) {
        Queue queue = new Queue(5);
        /*插入4个元素*/
        queue.insert(10);
        queue.insert(20);
        queue.insert(30);
        queue.insert(40);
        /*移除3个元素*/
        queue.remove();
        queue.remove();
        queue.remove();

        /*循环队列*/
        queue.insert(50);
        queue.insert(60);
        queue.insert(70);
        queue.insert(80);

        while (!queue.isEmpty()){
            long q = queue.remove();
            System.out.println(q);
        }
    }
}
