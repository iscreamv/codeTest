package com.code.queue;

/**
 * Java的队列实现：存放long类型数据
 */
public class Queue {

    private int maxSize;
    private long[] queArray;
    private int front;  //队首下标
    private int rear;   //队尾下标
    private int nItems; //队列中元素的数量

    public Queue(int maxSize) {
        this.maxSize = maxSize;
        queArray = new long[maxSize];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    /**
     * 把元素放在队尾
     *
     * @param j
     */
    public void insert(long j) {
        if (rear == maxSize - 1) {
            //rear到数组最后了，让队尾下标回到数组开头
            rear = -1;
        }
        queArray[++rear] = j;
        nItems++;
    }

    /**
     * 从队头移除元素
     *
     * @return
     */
    public long remove() {
        //获取值，并增加队头下标
        long temp = queArray[front++];
        if (front == maxSize) {
            //队头下标来到了数组末端，让其指向数组开头
            front = 0;
        }
        nItems--;
        return temp;
    }

    /**
     *
     * @return  队头元素
     */
    public long peekFront() {
        return queArray[front];
    }

    public boolean isEmpty() {
        return nItems == 0;
    }

    public boolean isFull(){
        return nItems == maxSize;
    }

    public int size(){
        return nItems;
    }
}
