package com.code.link.sortedList;

import com.code.link.LastLink.Link;

/**
 * 链表实现有序列表
 */
public class SortedList {
    private Link first;

    public SortedList() {
        first = null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void insert(long key) {
        Link newLink = new Link(key);
        //current从first开始
        Link previous = null;
        Link current = first;
        //如果previous指向列表最后的节点，current就指向null
        while (current != null && key > current.dData) {
            //后移一位
            previous = current;
            current = current.next;
        }
        if (previous == null) {
            //值最小，排在列表第一个
            first = newLink;
            newLink.next = current;
        } else {
            //插入previouse和current之间
            previous.next = newLink;
            newLink.next = current;
        }
    }

    /**
     * @return 返回并且删除第一个节点
     */
    public Link remove() {
        Link temp = first;
        first = first.next;
        return temp;
    }

    public void displayList() {
        System.out.print("List (first-->last): ");
        Link current = first;
        while (current != null) {
            current.displayLink();
            current = current.next;
        }
        System.out.println("");
    }
}
