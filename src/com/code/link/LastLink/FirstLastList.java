package com.code.link.LastLink;

/**
 * 第一个双端链表
 * 如果链表中只有一个链结点，first和last都指向它；
 * 如果没有链结点，两者都为null。
 */
public class FirstLastList {
    /**
     * 指向链表中的第一个节点
     */
    private Link first;

    /**
     * 指向最后一个节点
     */
    private Link last;

    public FirstLastList() {
        first = null;
        last = null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    /**
     * 插入链表第一个
     *
     * @param dd
     */
    public void insertFirst(long dd) {
        Link newLink = new Link(dd);
        if (isEmpty()) {
            //链表为空,last --> newLink
            last = newLink;
        }
        //newLink --> old first
        newLink.next = first;
        //first --> newLink
        first = newLink;
    }

    /**
     * 插到最后的节点
     *
     * @param dd
     */
    public void insertLast(long dd) {
        Link newLink = new Link(dd);
        if (isEmpty()) {
            //first --> newLink
            first = newLink;
        } else {
            //old last --> newLink
            last.next = newLink;
        }
        //last --> newLink
        last = newLink;
    }

    /**
     * 删除第一个节点
     */
    public long deleteFirst() {
        long temp = first.dData;
        if (first.next == null) {
            //只有一个节点
            last = null;
        }
        first = first.next;
        //last：指向最后节点的引用不用变
        return temp;
    }

    public void displayList() {
        System.out.print("List (first-->last): ");
        //从第一个节点开始遍历
        Link current = first;
        while (current != null) {
            current.displayLink();
            current = current.next;
        }
        System.out.println("");
    }

}
