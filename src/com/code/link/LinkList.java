package com.code.link;

/**
 * 只需要维护链表中第一个节点的引用
 */
public class LinkList {

    private Link first;

    public LinkList() {
        /*还没有元素*/
        first = null;
    }
    public boolean isEmpty(){
        return first == null;
    }

}
