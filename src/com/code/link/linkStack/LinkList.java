package com.code.link.linkStack;

public class LinkList {
    /**
     * 指向列表第一个节点
     */
    private Link first;

    public LinkList() {
        first = null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void insertFirst(Long dd) {
        Link newLink = new Link(dd);
        //newLink --> old first
        newLink.next = first;
        //first --> newLink
        first = newLink;
    }

    public long deleteFirst() {
        Link temp = first;
        first = first.next;
        return temp.dData;
    }

    public void displayList() {
        Link current = first;
        while (current != null) {
            current.displayLink();
            current = current.next;
        }
        System.out.println("");
    }


}
