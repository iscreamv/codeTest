package com.code.link.linkStack;

/**
 * 用链表实现的栈（后进先出）
 * 抽象数据类型（ADT）：着重于做了什么，忽略如何做的
 */
public class LinkStack {

    private LinkList theList;

    public LinkStack() {
        theList = new LinkList();
    }

    /**
     * 入栈，后入栈的永远在链表第一个
     *
     * @param j
     */
    public void push(long j) {
        theList.insertFirst(j);
    }

    /**
     * 出栈，取链表中第一个值
     *
     * @return
     */
    public long pop() {
        return theList.deleteFirst();
    }

    public boolean isEmpty() {
        return theList.isEmpty();
    }

    public void displayStack() {
        System.out.print("Stack (top-->bottom): ");
        theList.displayList();
    }
}
