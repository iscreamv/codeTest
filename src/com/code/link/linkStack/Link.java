package com.code.link.linkStack;

/**
 * 用于实现栈的链表节点
 */
public class Link {
    public long dData;
    public Link next;

    public Link(long dData) {
        this.dData = dData;
    }

    public void displayLink() {
        System.out.print(dData + " ");
    }
}
