package com.code.link.linkQueue;

import com.code.link.LastLink.FirstLastList;

/**
 * 用链表实现队列
 */
public class LinkQueue {
    //双端链表
    private FirstLastList theList;

    public LinkQueue() {
        theList = new FirstLastList();
    }

    public boolean isEmpty() {
        return theList.isEmpty();
    }

    /**
     * @param j 在队尾插入元素
     */
    public void insert(long j) {
        //注意是插入到链表的最后!!
        theList.insertLast(j);
    }

    /**
     * @return 返回队首元素
     */
    public long remove() {
        return theList.deleteFirst();
    }

    public void displayQueue() {
        System.out.print("Queue (front-->rear): ");
        theList.displayList();
    }
}
