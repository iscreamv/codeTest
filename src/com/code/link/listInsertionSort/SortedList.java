package com.code.link.listInsertionSort;

import com.code.link.LastLink.Link;

/**
 * 表插入排序，
 * 从无序数组中取出数据，然后插入有序链表，
 * 最后重新放入数组，得到有序数组
 */
public class SortedList {
    private Link first;

    public SortedList() {
        first = null;
    }

    /**
     * @param linkArr 无序数组
     */
    public SortedList(Link[] linkArr) {
        first = null;
        for (int j = 0; j < linkArr.length; j++) {
            insert(linkArr[j]);
        }
    }

    public void insert(Link k) {
        Link previous = null;
        Link current = first;
        while (current != null && k.dData > current.dData) {
            //后移一位
            previous = current;
            current = current.next;
        }
        if (previous == null) {
            first = k;
            k.next = current;
        } else {
            previous.next = k;
            k.next = current;
        }
    }

    public Link remove() {
        Link temp = first;
        first = first.next;
        return temp;
    }
}
