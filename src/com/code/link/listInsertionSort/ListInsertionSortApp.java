package com.code.link.listInsertionSort;

import com.code.link.LastLink.Link;

public class ListInsertionSortApp {
    public static void main(String[] args) {
        int size = 10;
        Link[] linkArr = new Link[size];
        for (int j = 0; j < size; j++) {
            int n = (int) (Math.random() * 99);
            Link newLink = new Link(n);
            linkArr[j] = newLink;
        }
        System.out.print("Unsorted array: ");
        for (int j = 0; j < size; j++) {
            System.out.print(linkArr[j].dData + " ");
        }
        System.out.println("");
        SortedList sortedList = new SortedList(linkArr);
        for (int j = 0; j < size; j++) {
            linkArr[j] = sortedList.remove();
        }
        System.out.print("Sorted array: ");
        for (int j = 0; j < size; j++) {
            System.out.print(linkArr[j].dData + " ");
        }
        System.out.println("");
    }
}
