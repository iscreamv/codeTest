package com.code.link;

public class Link {

    public int iData;
    public double dData;
    /**
     * 下一节点的引用
     */
    public Link next;

    public Link(int iData, double dData) {
        this.iData = iData;
        this.dData = dData;
    }

    @Override
    public String toString() {
        return "Link{" +
                "iData=" + iData +
                ", dData=" + dData +
                '}';
    }
}
