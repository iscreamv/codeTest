package com.code.link.iterator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IteratorApp {
    public static void main(String[] args) throws IOException {
        LinkList list = new LinkList();
        ListIterator iter1 = list.getIterator();
        long value;

        iter1.inserAfter(20);
        iter1.inserAfter(40);
        iter1.inserAfter(80);
        iter1.inserBefore(60);

        while (true) {
            System.out.print("输入show,rest,next,get,before,after,delete的首字母：");
            System.out.flush();
            char choice = getChar();
            switch (choice) {
                case 's':
                    //展示列表
                    if (!list.isEmpty()) {
                        list.displayList();
                    } else {
                        System.out.println("列表为空！");
                    }
                    break;
                case 'r':
                    //重置
                    iter1.reset();
                    break;
                case 'n':
                    //指向下一个结点
                    if (!list.isEmpty() && !iter1.atEnd()) {
                        //非空，并且不在最后
                        iter1.nextLink();
                    } else {
                        System.out.println("不能移到下一个结点");
                    }
                    break;
                case 'g':
                    //获取当前结点
                    if (!list.isEmpty()) {
                        value = iter1.getCurrent().dData;
                        System.out.println("返回值：" + value);
                    } else {
                        System.out.println("列表为空！");
                    }
                    break;
                case 'b':
                    //在当前结点前插入元素
                    System.out.print("输入要插入的值：");
                    System.out.flush();
                    value = getInt();
                    iter1.inserBefore(value);
                    break;
                case 'a':
                    System.out.print("输入要插入的值：");
                    System.out.flush();
                    value = getInt();
                    iter1.inserAfter(value);
                    break;
                case 'd':
                    //删除当前元素
                    if (!list.isEmpty()) {
                        value = iter1.deleteCurrent();
                        System.out.println("已删除：" + value);
                    } else {
                        System.out.println("列表为空，无法删除！");
                    }
                    break;
                default:
                    System.out.println("非法输入！");
            }
        }
    }

    public static String getString() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String s = br.readLine();
        return s;
    }

    public static char getChar() throws IOException {
        String s = getString();
        return s.charAt(0);
    }

    public static int getInt() throws IOException {
        String s = getString();
        return Integer.parseInt(s);

    }
}
