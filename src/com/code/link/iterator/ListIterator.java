package com.code.link.iterator;

import com.code.link.LastLink.Link;

/**
 * 迭代器类
 */
public class ListIterator {
    private Link current;
    private Link previous;
    private LinkList ourList;

    public ListIterator(LinkList ourList) {
        this.ourList = ourList;
    }

    /**
     * 把迭代器设在表头
     */
    public void reset() {
        current = ourList.getFirst();
        previous = null;
    }

    /**
     * @return 如果迭代器到达表尾，返回true
     */
    public boolean atEnd() {
        return current.next == null;
    }

    /**
     * 把迭代器移动到下一个结点
     */
    public void nextLink() {
        previous = current;
        current = current.next;
    }

    /**
     * 返回迭代器指向的结点
     *
     * @return
     */
    public Link getCurrent() {
        return current;
    }

    /**
     * @param dd 在迭代器后面插入新结点dd
     */
    public void inserAfter(long dd) {
        Link newLink = new Link(dd);
        if (ourList.isEmpty()) {
            ourList.setFirst(newLink);
            current = newLink;
        } else {
            newLink.next = current.next;
            current.next = newLink;
            //指向下一个结点
            nextLink();
        }
    }

    /**
     * @param dd 在迭代器前面插入新结点dd
     */
    public void inserBefore(long dd) {
        Link newLink = new Link(dd);
        if (previous == null) {
            newLink.next = ourList.getFirst();
            ourList.setFirst(newLink);
            reset();
        } else {
//            newLink.next = current;
            newLink.next = previous.next;
            previous.next = newLink;
            current = newLink;
        }
    }

    /**
     * @return 删除迭代器所指结点，并返回删除的值
     */
    public long deleteCurrent() {
        long value = current.dData;
        if (previous == null) {
            //在表头
            ourList.setFirst(current.next);
            reset();
        } else {
            previous.next = current.next;
            if (atEnd()) {
                reset();
            } else {
                current = current.next;
            }
        }
        return value;
    }
}
