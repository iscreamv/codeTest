package com.code.link.iterator;

import com.code.link.LastLink.Link;

public class LinkList {
    /**
     * 指向列表中的第一个结点
     */
    private Link first;

    public LinkList() {
        first = null;
    }

    public Link getFirst() {
        return first;
    }

    public void setFirst(Link link) {
        first = link;
    }

    public boolean isEmpty() {
        return first == null;
    }

    /**
     * @return 获取该列表的迭代器
     */
    public ListIterator getIterator() {
        return new ListIterator(this);
    }

    public void displayList() {
        Link current = first;
        while (current != null) {
            current.displayLink();
            current = current.next;
        }
        System.out.println("");
    }


}
