package com.code.link.doubleLinked;

/**
 * 双向链表的结点
 */
public class Link {
    public long dData;
    /**
     * 列表中的下一个结点
     */
    public Link next;
    public Link previous;

    public Link(long dData) {
        this.dData = dData;
    }

    public void displayLink() {
        System.out.print(dData + " ");
    }
}
