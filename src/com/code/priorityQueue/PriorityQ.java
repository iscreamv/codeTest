package com.code.priorityQueue;

/**
 * 优先级队列
 */
public class PriorityQ {

    //有序数组，最大值下标0，最小值下标size-1
    private int maxSize;
    private long[] queArray;
    private int nItems;

    public PriorityQ(int maxSize) {
        this.maxSize = maxSize;
        queArray = new long[maxSize];
        nItems = 0;
    }

    public void insert(long item) {
        int j;
        if (nItems == 0) {
            //无元素
            queArray[nItems++] = item;
        } else {
            for (j = nItems - 1; j >= 0; j--) {
                //从最后开始
                if (item > queArray[j]) {
                    //比新元素小的值后移一位，找到新元素应该插入的位置
                    queArray[j + 1] = queArray[j];
                } else {
                    //跳出循环
                    break;
                }
            }
            queArray[j + 1] = item;
            nItems++;
        }
    }

    /**
     * 移除最小值，删除元素也就是改变下标
     *
     * @return 最小值
     */
    public long remove() {
        return queArray[--nItems];
    }

    /**
     * 获取最小值，不删除元素
     *
     * @return 最小值
     */
    public long peekMin() {
        return queArray[nItems - 1];
    }


    public boolean isEmpty() {
        return nItems == 0;
    }

    public boolean isFull() {
        return nItems == maxSize;
    }

}
