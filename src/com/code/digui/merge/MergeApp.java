package com.code.digui.merge;

import java.util.Arrays;

/**
 * 归并两个有序数组
 */
public class MergeApp {

    public static void main(String[] args) {
        int[] arrayA = {23, 47, 81, 95};
        int[] arrayB = {7, 14, 39, 55, 62, 74};
        int[] arrayC = new int[10];
        merge(arrayA, 4, arrayB, 6, arrayC);
        display(arrayC);
    }

    public static void merge(int[] arrayA, int sizeA,
                             int[] arrayB, int sizeB,
                             int[] arrayC) {
        int aDex = 0, bDex = 0, cDex = 0;
        //两个数组都还没有遍历完
        while (aDex < sizeA && bDex < sizeB) {
            //把相对更小复制到C数组中
            if (arrayA[aDex] < arrayB[bDex]) {
                arrayC[cDex++] = arrayA[aDex++];
            } else {
                arrayC[cDex++] = arrayB[bDex++];
            }
        }
        //数组B遍历完，数组A还没有
        while (aDex < sizeA) {
            arrayC[cDex++] = arrayA[aDex++];
        }
        //数组A遍历完，数组B还没有
        while (bDex < sizeB) {
            arrayC[cDex++] = arrayB[bDex++];
        }
    }

    public static void display(int[] array) {
        Arrays.stream(array).forEach(value -> System.out.print(value + ","));
        System.out.println("");
    }
}
