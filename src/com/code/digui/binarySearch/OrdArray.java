package com.code.digui.binarySearch;

public class OrdArray {
    /**
     * 长整型数组a
     */
    private long[] a;
    private int nElems;

    public OrdArray(int max) {
        a = new long[max];
        nElems = 0;
    }

    public int size() {
        return nElems;
    }

    public int find(long searchKey) {
        return recFind(searchKey, 0, nElems - 1);
    }

    /**
     * @param searchKey  待查找的值
     * @param lowerBound 左边更小的下标
     * @param upperBound 右边更大的下标
     * @return 值所在下标
     */
    public int recFind(long searchKey, int lowerBound, int upperBound) {
        //当前下标
        int curIn;
        curIn = (lowerBound + upperBound) / 2;
        if (a[curIn] == searchKey) {
            return curIn;
        } else if (lowerBound > upperBound) {
            //没找到
            return nElems;
        } else {
            if (a[curIn] < searchKey) {
                //在值更大的右半部分
                return recFind(searchKey, curIn + 1, upperBound);
            } else {
                return recFind(searchKey, lowerBound, curIn - 1);
            }
        }
    }

    /**
     * @param value 将value插入有序数组
     */
    public void insert(long value) {
        //value下标
        int j;
        //1、找到value应该插入的位置，即比value更大值所在的下标
        for (j = 0; j < nElems; j++) {
            if (a[j] > value) {
                //跳出循环，此时a[j]就是应该插入的位置
                break;
            }
        }
        //2、把所有更大的值右移一位
        for (int k = nElems; k > j; k--) {
            a[k] = a[k - 1];
        }
        //3、上面循环结束的时候，a[j] == a[j+1]
        a[j] = value;
        nElems++;
    }

    public void display() {
        for (int j = 0; j < nElems; j++) {
            System.out.print(a[j] + " ");
        }
        System.out.println("");
    }
}
