package com.code.digui.binarySearch;

/**
 * 使用递归实现的二分查找法
 */
public class BinarySearch {
    public static void main(String[] args) {
        int maxSize = 100;
        OrdArray arr = new OrdArray(maxSize);
        arr.insert(72);
        arr.insert(90);
        arr.insert(45);
        arr.insert(126);
        arr.insert(54);
        arr.insert(99);
        arr.insert(144);
        arr.insert(27);
        arr.insert(135);
        arr.insert(81);
        arr.insert(18);
        arr.insert(108);
        arr.insert(9);
        arr.insert(117);
        arr.insert(63);
        arr.insert(36);
        arr.display();

        int key = 27;
        if (arr.find(key) != arr.size()) {
            System.out.println("找到值" + key + "，下标" + arr.find(key));
        } else {
            System.out.println("没找到" + key);
        }
    }
}
