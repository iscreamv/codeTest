package com.code.stack.demo2;

public class BracketApp {

    public static void main(String[] args) {
        String str = "a(b{cd}";
        BracketChecker checker = new BracketChecker(str);
        checker.check();
        System.out.println("---分割线---");
        str = "a(b{cd)";
        checker = new BracketChecker(str);
        checker.check();
        System.out.println("---分割线---");
        str = "a(b{cd)e)";
        checker = new BracketChecker(str);
        checker.check();
        System.out.println("---分割线---");
        str = "a(b{cd)a)b]";
        checker = new BracketChecker(str);
        checker.check();
    }
}
