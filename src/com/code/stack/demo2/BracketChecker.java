package com.code.stack.demo2;


import com.code.stack.demo1.Stack1;

public class BracketChecker {
    private String input;

    public BracketChecker(String input) {
        this.input = input;
    }

    public void check() {
        int stackSize = input.length();
        Stack1 theStack = new Stack1(stackSize);
        for (int j = 0; j < input.length(); j++) {
            char c = input.charAt(j);
            switch (c) {
                //char�����õ�����
                case '{':
                case '[':
                case '(':
                    //��ָ�����ջ
                    theStack.push(c);
                    break;
                case '}':
                case ']':
                case ')':
                    if (!theStack.isEmpty()) {
                        char pop = theStack.pop();
                        if ((c == '}' && pop != '{') ||
                                (c == ']' && pop != '[') ||
                                (c == ')' && pop != '(')) {
                            System.out.println("Error:�ҷָ���" + c + "��ƥ��" + ",at" + j);
                        }
                    } else {
                        //��ָ���ջ�ѿ�
                        System.out.println("Error:�ҷָ���" + c + "��ƥ��" + ",at" + j);
                    }
                    break;
                default:
                    break;
            }
        }
        //forѭ���������ж�ջ�Ƿ���ʣ��Ԫ��
        if (!theStack.isEmpty()) {
            System.out.println("Error:��ʧ�ҷָ���");
        }
    }
}
