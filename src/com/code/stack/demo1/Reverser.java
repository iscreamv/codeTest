package com.code.stack.demo1;

public class Reverser {

    private String input;
    private String output;

    public Reverser(String input) {
        this.input = input;
    }

    /**
     * �������
     *
     * @return
     */
    public String doRev() {
        int stackSize = input.length();
        Stack1 stack1 = new Stack1(stackSize);
        for (int j = 0; j < input.length(); j++) {
            char c = input.charAt(j);
            stack1.push(c);
        }

        output = "";

        while (!stack1.isEmpty()) {
            char pop = stack1.pop();
            output += pop;
        }
        return output;
    }
}
