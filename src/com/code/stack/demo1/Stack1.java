package com.code.stack.demo1;

public class Stack1 {
    /**
     * 栈元素最大个数
     */
    private int maxSize;

    private char[] stackArray;

    /**
     * 栈顶元素下标
     */
    private int top;

    public Stack1(int s) {
        //设置栈最大值
        maxSize = s;
        //初始化数组
        stackArray = new char[maxSize];
        //还没有元素
        top = -1;
    }

    /**
     * 入栈，重点是top是在插入之前递增的
     *
     * @param j -- 栈顶元素
     */
    public void push(char j) {
        stackArray[++top] = j;
    }

    /**
     * 出栈，先返回top元素，然后top减1
     *
     * @return 栈顶元素
     */
    public char pop() {
        return stackArray[top--];
    }

    /**
     * 查看
     *
     * @return 栈顶元素
     */
    public char peek() {
        return stackArray[top];
    }

    public boolean isEmpty() {
        return top == -1;
    }

    public boolean isFull() {
        return top == maxSize - 1;
    }
}
