package com.code.orderArray;

public class OrderedApp {
    public static void main(String[] args) {
        int maxSize = 100;    //插入的数量超出这个大小，则会报java.lang.ArrayIndexOutOfBoundsException
        OrderArray arr = new OrderArray(maxSize);
        arr.insert(77);
        arr.insert(99);
        arr.insert(44);
        arr.insert(55);
        arr.insert(22);

        arr.display();

        int searchKey = 55;
        if (arr.find(searchKey) != arr.size()) {
            System.out.println("找到数：" + searchKey);
        } else {
            System.out.println("未找到：" + searchKey);
        }

        arr.delete(55);
        arr.delete(22);
        arr.display();
    }
}
