package com.code.rbTree;

public class RBTree<T extends Comparable<T>> {

    //根节点
    private RBNode<T> root;
    //定义红黑树标志
    private static final boolean RED = false;
    private static final boolean BLACK = true;

    //内部类
    public class RBNode<T extends Comparable<T>> {
        boolean color;//颜色
        T key;//关键值
        RBNode<T> left;//左子节点
        RBNode<T> right;//右子节点
        RBNode<T> parent;//父节点

        public RBNode(boolean color, T key, RBNode<T> left, RBNode<T> right, RBNode<T> parent) {
            this.color = color;
            this.key = key;
            this.left = left;
            this.right = right;
            this.parent = parent;
        }

        public T getKey() {
            return key;
        }

        @Override
        public String toString() {
            return "" + key + (this.color == RED ? "R" : "B");
        }
    }

    public RBTree() {
        root = null;
    }

    public RBNode<T> parentOf(RBNode<T> node) { //获取父节点
        return node != null ? node.parent : null;
    }

    public void setParent(RBNode<T> node, RBNode<T> parent) {   //设置父节点
        if (node != null) {
            node.parent = parent;
        }
    }


}
