package com.code.selectSort;

import java.util.Arrays;

public class ArraySel {
    private long[] a;
    /**
     * number of data items
     */
    private int nElems;

    public ArraySel(int max) {
        a = new long[max];
        nElems = 0;
    }

    public void insert(long value) {
        a[nElems] = value;
        nElems++;
    }

    public void display() {
        Arrays.stream(a).filter(value -> value > 0)
                .forEach(value -> {
                    System.out.print(value + " ");
                });
        System.out.println();
    }

    public void selectionSort() {
        /*out：外循环下标；
         * in：内循环下标；
         * min：最小值下标*/
        int out, in, min, compareCount = 0, swapCount = 0;
        for (out = 0; out < nElems - 1; out++) {
            min = out;
            for (in = out + 1; in < nElems; in++) {
                compareCount++;
                if (a[in] < a[min]) {
                    min = in;
                }
            }
            /*内循环结束，交换最小值*/
            swapCount++;
            swap(out, min);
        }
        System.out.println("比较次数：" + compareCount);
        System.out.println("交换次数：" + swapCount);
    }

    private void swap(int one, int two) {
        long temp = a[one];
        a[one] = a[two];
        a[two] = temp;
    }
}
