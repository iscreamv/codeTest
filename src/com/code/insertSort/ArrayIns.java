package com.code.insertSort;

import java.util.Arrays;

public class ArrayIns {

    private long[] a;
    /**
     * number of data items
     */
    private int nElems;

    public ArrayIns(int max) {
        a = new long[max];
        nElems = 0;
    }

    public void insert(long value) {
        a[nElems] = value;
        nElems++;
    }

    public void display() {
        Arrays.stream(a).filter(value -> value > 0)
                .forEach(value -> {
                    System.out.print(value + " ");
                });
        System.out.println();
    }

    public void insertionSort() {
        int in, out;
        /*out从1开始，标记未排序的最左端部分*/
        for (out = 1; out < nElems; out++) {
            long temp = a[out];
            in = out;
            while (in > 0 && a[in - 1] > temp) {
                /*所有大于temp的值右移一位*/
                a[in] = a[in - 1];
                in--;
            }
            a[in] = temp;
        }
    }
}
