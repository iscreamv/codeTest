package com.code.bubbleSort;

import java.util.Arrays;

public class ArrayBub {
    private long[] a;
    /**
     * number of data items
     */
    private int nElems;

    public ArrayBub(int max) {
        a = new long[max];
        //一开始还没有元素
        nElems = 0;
    }

    public void insert(long value) {
        a[nElems] = value;
        nElems++;
    }

    public void display() {
        Arrays.stream(a).filter(value -> value > 0)
                .forEach(value -> {
                    System.out.print(value + " ");
                });
        System.out.println();
    }

    public void bubbleSort() {
        int first, last, count = 0;
        for (last = nElems - 1; last > 0; last--) {
            for (first = 0; first < last; first++) {
                count++;
                if (a[first] > a[first + 1]) {
                    swap(first, first + 1);
                }
            }
        }
        System.out.println("总比较次数=" + count);
    }

    /**
     * 交换
     *
     * @param one
     * @param two
     */
    private void swap(int one, int two) {
        long temp = a[one];
        a[one] = a[two];
        a[two] = temp;
    }
}
