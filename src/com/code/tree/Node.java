package com.code.tree;

public class Node {
    public int iData;
    public double dData;
    /**
     * 左子节点
     */
    public Node leftChild;
    /**
     * 右子节点
     */
    public Node rightChild;

    public void displayNode() {
        System.out.print("{" + iData + "," + dData + "} ");
    }
}
