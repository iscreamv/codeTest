package com.code.tree;

import java.util.Stack;

public class Tree {
    /**
     * 根节点
     */
    private Node root;

    public Tree() {
        root = null;
    }

    public Node find(int key) {
        Node current = root;
        while (current.iData != key) {
            if (key < current.iData) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }
            //没有子节点，即未找到指定key
            if (current == null) {
                return null;
            }
        }
        return current;
    }

    public void insert(int id, double dd) {
        Node newNode = new Node();
        newNode.iData = id;
        newNode.dData = dd;
        if (root == null) {
            root = newNode;
        } else {
            //从根开始遍历
            Node current = root;
            //存放父节点
            Node parrent;

            while (true) {
                parrent = current;
                if (id < current.iData) {
                    current = current.leftChild;
                    if (current == null) {
                        parrent.leftChild = newNode;
                        return;
                    }
                } else {
                    current = current.rightChild;
                    if (current == null) {
                        parrent.rightChild = newNode;
                        return;
                    }
                }
            }
        }
    }

    public boolean delete(int key) {
        Node current = root;
        Node parent = root;
        //记录相对于上一个节点是否是左子节点
        boolean isLeftChild = true;
        //查找节点
        while (current.iData != key) {
            parent = current;
            //current指向下一个节点
            if (key < current.iData) {
                //向左
                isLeftChild = true;
                current = current.leftChild;
            } else {
                isLeftChild = false;
                current = current.rightChild;
            }

            //遍历到最后，没有找到
            if (current == null) {
                return false;
            }
        }
        //1.如果没有子节点，只要简单的删除它
        if (current.leftChild == null && current.rightChild == null) {
            if (current == root) {
                //根节点，清空树
                root = null;
            } else if (isLeftChild) {
                //左节点，删除父节点的左节点
                parent.leftChild = null;
            } else {
                parent.rightChild = null;
            }
        } else if (current.rightChild == null) {
            //2.如果没有右节点，用左边的子树来代替
            if (current == root) {
                //根节点
                root = current.leftChild;
            } else if (isLeftChild) {
                parent.leftChild = current.leftChild;
            } else {
                parent.rightChild = current.leftChild;
            }
        } else if (current.leftChild == null) {
            //3.如果没有左子节点，用右子树来代替
            if (current == root) {
                root = current.rightChild;
            } else if (isLeftChild) {
                parent.leftChild = current.rightChild;
            } else {
                parent.rightChild = current.rightChild;
            }
        } else {
            //4.拥有两个子节点，找比current的后继节点，也就是要找比current大、比右边其他节点都要小的节点，
            //用人眼观察，就是找右边子树的最下面、最左边的节点
            Node successor = getSuccessor(current);

            //把要删除的current的父节点连接到successor后继节点上
            if (current == root) {
                root = successor;
            } else if (isLeftChild) {
                parent.leftChild = successor;
            } else {
                parent.rightChild = successor;
            }

            //把successor连接到current的左子节点
            successor.leftChild = current.leftChild;
        }
        return true;
    }

    /**
     * 找delNode的右边的节点，遍历右子节点的左边的后代
     *
     * @param delNode
     * @return
     */
    private Node getSuccessor(Node delNode) {
        //后代的父母
        Node successorParent = delNode;
        //后代
        Node successor = delNode;
        //当前节点，移向右子节点
        Node current = delNode.rightChild;

        //遍历所有的左子节点
        while (current != null) {
            successorParent = successor;
            successor = current;
            //指向左子节点
            current = current.leftChild;
        }

        //如果后继节点不是被删除节点的右子节点，连接后继节点的父节点和子节点
        if (successor != delNode.rightChild) {
            successorParent.leftChild = successor.rightChild;
            //这里后继节点的右子节点已经指定了
            successor.rightChild = delNode.rightChild;
        }

        return successor;
    }

    /**
     * @param traverseType 遍历类型
     */
    public void traverse(int traverseType) {
        switch (traverseType) {
            case 1:
                System.out.print("PreOrder traversal:  ");
                preOrder(root);
                break;
            case 2:
                System.out.print("InOrder traversal:  ");
                inOrder(root);
                break;
            case 3:
                System.out.print("PostOrder traversal:  ");
                postOrder(root);
                break;
            default:
                break;
        }
        System.out.println("");
    }

    /**
     * 前序遍历
     *
     * @param localRoot
     */
    private void preOrder(Node localRoot) {
        if (localRoot != null) {
            System.out.print(localRoot.iData + " ");
            preOrder(localRoot.leftChild);
            preOrder(localRoot.rightChild);
        }
    }

    /**
     * 所有节点按关键字升序访问
     *
     * @param localRoot 中序遍历
     */
    private void inOrder(Node localRoot) {
        if (localRoot != null) {
            inOrder(localRoot.leftChild);
            System.out.print(localRoot.iData + " ");
            inOrder(localRoot.rightChild);
        }
    }

    /**
     * 后序遍历
     *
     * @param localRoot
     */
    private void postOrder(Node localRoot) {
        if (localRoot != null) {
            postOrder(localRoot.leftChild);
            postOrder(localRoot.rightChild);
            System.out.print(localRoot.iData + " ");
        }
    }


    public void displayTree() {
        Stack globalStack = new Stack();
        //根节点入栈
        globalStack.push(root);
        int nBlanks = 32;
        boolean isRowEmpty = false;
        System.out.println("................................................");
        while (isRowEmpty == false) {
            Stack localStack = new Stack();
            isRowEmpty = true;
            for (int j = 0; j < nBlanks; j++) {
                System.out.print(" ");
            }
            while (globalStack.isEmpty() == false) {
                Node temp = (Node) globalStack.pop();
                if (temp != null) {
                    System.out.print(temp.iData);
                    localStack.push(temp.leftChild);
                    localStack.push(temp.rightChild);
                    if (temp.leftChild != null || temp.rightChild != null) {
                        isRowEmpty = false;
                    }
                } else {
                    System.out.print("--");
                    localStack.push(null);
                    localStack.push(null);
                }
                for (int j = 0; j < nBlanks * 2 - 2; j++) {
                    System.out.print(" ");
                }
            }

            System.out.println("");
            nBlanks /= 2;
            while (localStack.isEmpty() == false) {
                globalStack.push(localStack.pop());
            }
        }

        System.out.println("................................................");
    }
}
